from job.jobetl import JobETL

"""
    Para inicar o processo de ETL é preciso executar a classe main 
    e informar os dados do banco de dados de destino na variável dbconfig.
"""

if __name__ == "__main__":

    """
        O dicionário dbconfig armazena as informações de conexão 
        com o banco de dados destino.
            - database: nome no banco de dados.
            - user: usuário para conectar.
            - password: senha informada no ato da conexão.
            - host: endereço IP da máquina que possui a instância do banco de dados.
            - port: porta disponibilizada para conexão.
    """
    dbconfig = {
        'database': 'etl',
        'user': 'root',
        'password': '159713',
        'host': '127.0.0.1',
        'port':'3306'
    }

    """
     O JobETL realiza o proceso de Estração, Transformação  
     e Carga de dados nas dimensões da estrutura do DW proposto.
     O JobETL recebe como parâmetros o local e dbconfig.
     Através no parâmetro local é possível executar o JobETL em 
     cluter precisando apenas informar o IP da máquina como parâmetro.
      
    """
    JobETL('local', dbconfig)


























    #from mysql.connector import (connection)

