from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, FloatType, StringType, DateType
from connection.connectDb import ConnectDbMySQL

"""
    A classe JobETL possui a implementação de transformações 
    que serão utilizadas para fazer o ETL do dados. Esta classe inicializa 
    o Spark para fazer o processamento de dados.
"""

class JobETL:
    def __init__(self, host, dbconfig):
        self.host = host
        self.spark = SparkSession.builder \
            .master(self.host) \
            .appName("ETL") \
            .config("spark.some.config.option", "some-value") \
            .getOrCreate()
        self.conn = ConnectDbMySQL(dbconfig['user'], dbconfig['password'], dbconfig['database'], dbconfig['host'],
                                   dbconfig['port']).start()
        self.start()

    """
        O método start realiza chamadas para execursão dos método 
        de ETL que irão inserir registros das dimensões do DW.
    """
    def start(self):
        print('Starting ...')

        self.dim_payment_type()
        self.dim_category_product()
        self.dim_geolocation()
        self.dim_localization()
        self.dim_customer()
        self.dim_product()
        self.dim_seller()
        self.dim_order_status()
        self.fact_order()

    """
        O método dim_payment_type faz a insersão de registro de 
        tipos de pagamentos na dimensões dim_payment_type do DW.
    """
    def dim_payment_type(self):
        print('Starting to extract data for dimension dim_payment_type')

        schema_payment_type = StructType([
            StructField("order_id", StringType(), True),
            StructField("payment_sequential", IntegerType(), True),
            StructField("payment_type", StringType(), True),
            StructField("payment_installments", IntegerType(), True),
            StructField("payment_value", FloatType(), True)
        ])

        df_payment_type = self.spark.read.csv('../etl/datasets/olist_order_payments_dataset.csv', header=False,
                                              schema=schema_payment_type) \
            .drop_duplicates(subset=['payment_type']).na.drop(how="all")

        stmt = """
                   insert into dim_payment_type(payment_type_description)
                   values (%(payment_type)s)
                """

        listPaymentType = []
        for rowPaymentType in df_payment_type.collect():
            listPaymentType.append({'payment_type': rowPaymentType.payment_type})

        self.conn.cursor().executemany(stmt, listPaymentType)
        self.conn.commit()

        print('Records successfully inserted')

    """
        O método dim_category_product faz a insersão de registro de 
        categoria de produtos no DW.
    """
    def dim_category_product(self):

        print('Starting to extract data for dimension dim_category_product')

        schema_category_product = StructType([
            StructField("product_category_name", StringType(), True),
            StructField("product_category_name_english", StringType(), True)
        ])

        df_category_product = self.spark.read.csv("../etl/datasets/product_category_name_translation.csv",
                                                  schema=schema_category_product, header=False).na.drop(how="all")

        listCategoryProduct = []

        for categoryProdutct in df_category_product.collect():
            listCategoryProduct.append({'category_product_name': categoryProdutct.product_category_name,
                                        'product_category_name_english': categoryProdutct.product_category_name_english})

        stmt = """
          Insert Into dim_category_product(category_product_name,category_product_name_english)
          Values (%(category_product_name)s, %(product_category_name_english)s)
          """

        self.conn.cursor().executemany(stmt, listCategoryProduct)
        self.conn.commit()
        print('Records successfully inserted')

    """
        O método dim_geolocation faz a insersão de registro de 
        geolocalização no DW.
    """
    def dim_geolocation(self):
        print('Starting to extract data for dimension dim_geolocation')

        schema_geolocation = StructType([
            StructField("geolocation_zip_code_prefix", IntegerType(), True),
            StructField("geolocation_lat", FloatType(), True),
            StructField("geolocation_lng", FloatType(), True)
        ])

        df_geolocation = self.spark.read.csv("../etl/datasets/olist_geolocation_dataset.csv", schema=schema_geolocation,
                                             header=False).dropna().dropDuplicates(
            subset=['geolocation_zip_code_prefix'])

        listGeolocation = []

        for geolocation in df_geolocation.collect():
            listGeolocation.append({'geolocation_zip_code_prefix': geolocation.geolocation_zip_code_prefix,
                                    'geolocation_lat': geolocation.geolocation_lat,
                                    'geolocation_lng': geolocation.geolocation_lng})
        stmt = """
                Insert Into dim_geolocation(geolocation_lat,geolocation_lng, geolocation_zip_code_prefix_sk)
                Values (%(geolocation_lat)s, %(geolocation_lng)s,  %(geolocation_zip_code_prefix)s)
                """
        self.conn.cursor().executemany(stmt, listGeolocation)
        self.conn.commit()

        print('Records successfully inserted')

    """
        O método dim_localization faz a insersão de registro de 
        localizaçao no DW.
    """
    def dim_localization(self):
        print('Starting to extract data for dimension dim_localization')

        schema_localization = StructType([
            StructField("customer_id", StringType(), True),
            StructField("customer_unique_id", StringType(), True),
            StructField("customer_zip_code_prefix", IntegerType(), True),
            StructField("customer_city", StringType(), True),
            StructField("customer_state", StringType(), True)
        ])
        df_localization = self.spark.read.csv("../etl/datasets/olist_customers_dataset.csv",
                                              header=False, schema=schema_localization).dropna().dropDuplicates(
            subset=['customer_city'])

        listLocalization = []

        for localization in df_localization.collect():
            listLocalization.append({
                'localization_zip_code_prefix': localization.customer_zip_code_prefix,
                'localization_city': localization.customer_city,
                'localization_state': localization.customer_state
            })

        stmt = """
                       Insert Into dim_localization(localization_zip_code_prefix,localization_city, localization_state)
                       Values (%(localization_zip_code_prefix)s, %(localization_city)s,  %(localization_state)s)
                """
        self.conn.cursor().executemany(stmt, listLocalization)
        self.conn.commit()


        print('Records successfully inserted')

    """
        O método dim_customer faz a insersão de registro de 
        clientes no DW.
    """
    def dim_customer(self):

        print('Starting to extract data for dimension dim_customer')

        schema_customer = StructType([
            StructField("customer_id", StringType(), True),
            StructField("customer_unique_id", StringType(), True),
            StructField("customer_zip_code_prefix", IntegerType(), True),
            StructField("customer_city", StringType(), True),
            StructField("customer_state", StringType(), True)
        ])
        df_customer = self.spark.read.csv("../etl/datasets/olist_customers_dataset.csv",
                                          header=False, schema=schema_customer).dropna().dropDuplicates(subset=['customer_id'])
        df_customer = df_customer.select("customer_id", "customer_zip_code_prefix")


        dimLocalization = self.spark.createDataFrame(self.listAllDimLocalication() \
                                                       , ["localization_id", "customer_zip_code_prefix"])
        dimGeolocation = self.spark.createDataFrame(self.lisAllDimGeolocation() \
                                                     , ["geolocation_id", "customer_zip_code_prefix"])

        dimCustomer = df_customer\
            .join(dimLocalization, 'customer_zip_code_prefix', 'left')\
            .join(dimGeolocation, 'customer_zip_code_prefix', 'left')\
            .dropDuplicates(subset=['customer_id'])

        listCustomer = []

        for rowCustomer in dimCustomer.collect():
            listCustomer.append({
                "customer_sk":rowCustomer.customer_id,
                "localization_id":rowCustomer.localization_id,
                "geolocation_id":rowCustomer.geolocation_id
            })

        stmt = """
                    Insert Into dim_customer(customer_sk, localization_id, geolocation_id)
                            Values (%(customer_sk)s, %(localization_id)s,%(geolocation_id)s)
                     """
        self.conn.cursor().executemany(stmt, listCustomer)
        self.conn.commit()

        print('Records successfully inserted')
        return 1

    """
        O método dim_product faz a insersão de registro de 
        produtos no DW.
    """
    def dim_product(self):

        print('Starting to extract data for dimension dim_product')

        schema_product = StructType([
            StructField("product_id", StringType(), True),
            StructField("product_category_name", StringType(), True),
            StructField("product_name_lenght", IntegerType(), True),
            StructField("product_description_lenght", IntegerType(), True),
            StructField("product_photos_qty", IntegerType(), True),
            StructField("product_weight_g", FloatType(), True),
            StructField("product_length_cm", FloatType(), True),
            StructField("product_height_cm", FloatType(), True),
            StructField("product_width_cm", FloatType(), True)
        ])

        df_product = self.spark.read.csv("../etl/datasets/olist_products_dataset.csv",
                                         header=False, schema=schema_product).dropna().dropDuplicates(subset=['product_id'])

        df_category_product = self.spark.createDataFrame(self.listAllDimCategoryProduct(), ["category_product_id","product_category_name"])

        dimProduc = df_product.join(df_category_product, 'product_category_name', 'left')

        listProduct = []

        for produc in dimProduc.collect():
            listProduct.append({"product_sk":produc.product_id,
                                "category_product_id":produc.category_product_id,
                                "product_name_lenght":produc.product_name_lenght,
                                "product_description_lenght":produc.product_description_lenght,
                                "product_photos_qty":produc.product_photos_qty,
                                "product_weight_g":produc.product_weight_g,
                                "product_length_cm":produc.product_length_cm,
                                "product_height_cm":produc.product_height_cm,
                                "product_width_cm":produc.product_width_cm
                                })

        stmt = """
                Insert Into dim_product(product_sk, category_product_id, product_name_lenght,product_description_lenght,
                product_photos_qty, product_weight_g,product_length_cm,product_height_cm,product_width_cm)
                    Values (%(product_sk)s, %(category_product_id)s,%(product_name_lenght)s, %(product_description_lenght)s,
                             %(product_photos_qty)s,%(product_weight_g)s,%(product_length_cm)s,%(product_height_cm)s,
                            %(product_width_cm)s )
                """

        self.conn.cursor().executemany(stmt, listProduct)
        self.conn.commit()

        print('Records successfully inserted')

    """
        O método dim_seller faz a insersão de registro de 
        vendedores no DW.
    """
    def dim_seller(self):
        schema_seller = StructType([
            StructField("seller_id", StringType(), True),
            StructField("seller_zip_code_prefix", IntegerType(), True),
            StructField("seller_city", StringType(), True),
            StructField("seller_state", StringType(), True)
        ])
        df_seller = self.spark.read.csv("../etl/datasets/olist_sellers_dataset.csv",
                                        header=False, schema=schema_seller).dropna().dropDuplicates(subset=['seller_id'])

        df_dim_localization = self.spark.createDataFrame(self.listAllDimLocalication()\
                                                         ,["localization_id", "seller_zip_code_prefix"])
        df_fim_geolocation = self.spark.createDataFrame(self.lisAllDimGeolocation()\
                                                        ,["geolocation_id", "seller_zip_code_prefix"])

        dimSeller = df_seller\
            .join(df_dim_localization,'seller_zip_code_prefix', 'left')\
            .join(df_fim_geolocation,'seller_zip_code_prefix', 'left')\
            .dropDuplicates(subset=['seller_id'])

        listSeller = []

        for seller in dimSeller.collect():
            listSeller.append({
                "seller_sk":seller.seller_id,
                "localization_id":seller.localization_id,
                "geolocation_id":seller.geolocation_id
            })
        stmt = """
                    Insert Into dim_seller(seller_sk, localization_id, geolocation_id)
                          Values (%(seller_sk)s, %(localization_id)s,%(geolocation_id)s)
                """

        self.conn.cursor().executemany(stmt, listSeller)
        self.conn.commit()

        print('Records successfully inserted')

    """
        O método dim_order_status faz a insersão de registro de 
        estatus usados na ordem das compras no DW.
    """
    def dim_order_status(self):
        schema_order_status = StructType([
            StructField("order_id", StringType(), True),
            StructField("customer_id", StringType(), True),
            StructField("order_status", StringType(), True),
            StructField("order_purchase_timestamp", DateType(), True),
            StructField("order_approved_at", DateType(), True),
            StructField("order_delivered_carrier_date", DateType(), True),
            StructField("order_delivered_customer_date", DateType(), True),
            StructField("order_estimated_delivery_date", DateType(), True)
        ])

        df_order_status = self.spark.read.csv("../etl/datasets/olist_orders_dataset.csv", header=False,
                                              schema=schema_order_status) \
            .dropDuplicates(subset=['order_status']).na.drop(how="all")

        listStatusOrder = []

        for statusOrder in df_order_status.collect():
            listStatusOrder.append({
                "order_status_description":statusOrder.order_status
            })
        stmt = """
                Insert Into dim_order_status(order_status_description)
                    Values (%(order_status_description)s)
                """

        self.conn.cursor().executemany(stmt, listStatusOrder)
        self.conn.commit()

        print('Records successfully inserted')

    """
        O método fact_order é responsável em fazer a insersão 
        e relacionamentos das entidas na tabela fato do DW.
    """
    def fact_order(self):
        print('Starting to extract data for  fact_order')

        schema_order = StructType([
            StructField("order_id", StringType(), True),
            StructField("customer_id", StringType(), True),
            StructField("order_status", StringType(), True),
            StructField("order_purchase_timestamp", DateType(), True),
            StructField("order_approved_at", DateType(), True),
            StructField("order_delivered_carrier_date", DateType(), True),
            StructField("order_delivered_customer_date", DateType(), True),
            StructField("order_estimated_delivery_date", DateType(), True)
        ])

        schema_order_payments = StructType ([
            StructField("order_id", StringType(), True),
            StructField("payment_sequential", IntegerType(), True),
            StructField("payment_type", StringType(), True),
            StructField("payment_installments", IntegerType(), True),
            StructField("payment_value", FloatType(), True),
        ])

        schema_order_item = StructType([
            StructField("order_id", StringType(), True),
            StructField("order_item_id", IntegerType(), True),
            StructField("product_id", StringType(), True),
            StructField("seller_id", StringType(), True),
            StructField("shipping_limit_date", DateType(), True),
            StructField("price", FloatType(), True),
            StructField("freight_value", FloatType(), True),
        ])

        df_order = self.spark.read.csv("../etl/datasets/olist_orders_dataset.csv", header=False, schema=schema_order) \
            .dropDuplicates(subset=['order_id']).na.drop(how="all")

        df_order_payments = self.spark.read.csv("../etl/datasets/olist_order_payments_dataset.csv"\
                                                , header=False, schema=schema_order_payments )\
                                                .dropDuplicates(subset=['order_id']).na.drop(how="all")

        df_order_items = self.spark.read.csv("../etl/datasets/olist_order_items_dataset.csv"\
                                                , header=False, schema=schema_order_item)\
                                                .dropDuplicates(subset=['order_id']).na.drop(how="all")

        DimOrderStatus = self.spark.createDataFrame(self.listAllDimOrderStatur(), ['order_status_id', 'order_status'])
        DimProduct = self.spark.createDataFrame(self.listAllDimProduct(), ['product_product_id', 'product_id'])
        DimSeller = self.spark.createDataFrame(self.listAllDimSeller(), ['seller_seller_id','seller_id'])
        DimCustomer = self.spark.createDataFrame(self.listAllDimCustomer(), ['customer_customer_id', 'customer_id'])
        DimPaymentType = self.spark.createDataFrame(self.listAllDimPaymentType(), ['payment_type_id', 'payment_type'])
        timeShippingLimitDate = self.spark.createDataFrame(self.listAllDimTime(), ['shipping_limit_date_id', 'shipping_limit_date'])
        timeOrderPurchaseTimestamp = self.spark.createDataFrame(self.listAllDimTime(), ['order_purchase_timestamp_id', 'order_purchase_timestamp'])
        timeOrderApprovedAt = self.spark.createDataFrame(self.listAllDimTime(), ['order_approved_at_id', 'order_approved_at'])
        timeOrderDeliveredCarrierDate = self.spark.createDataFrame(self.listAllDimTime(), ['order_delivered_carrier_date_id', 'order_delivered_carrier_date'])
        timeOrderDeliveredCustomerDate = self.spark.createDataFrame(self.listAllDimTime(), ['order_delivered_customer_date_id', 'order_delivered_customer_date'])
        timeOrderEstimatedDeliveryDate = self.spark.createDataFrame(self.listAllDimTime(), ['order_estimated_delivery_date_id', 'order_estimated_delivery_date'])

        df_fact_order = df_order_payments\
            .join(df_order_items,'order_id','inner')\
            .join(df_order,'order_id','inner') \
            .join(DimOrderStatus, 'order_status', 'left')\
            .join(DimProduct, 'product_id', 'inner') \
            .join(DimSeller, 'seller_id', 'left')\
            .join(DimCustomer, 'customer_id', 'left') \
            .join(DimPaymentType, 'payment_type', 'left') \
            .join(timeShippingLimitDate, 'shipping_limit_date', 'left') \
            .join(timeOrderPurchaseTimestamp, 'order_purchase_timestamp', 'left') \
            .join(timeOrderApprovedAt, 'order_approved_at', 'left') \
            .join(timeOrderDeliveredCarrierDate, 'order_delivered_carrier_date', 'left') \
            .join(timeOrderDeliveredCustomerDate, 'order_delivered_customer_date', 'left')\
            .join(timeOrderEstimatedDeliveryDate, 'order_estimated_delivery_date', 'left')

        df_fact_order = df_fact_order.select("order_id", "order_item_id", "price","freight_value","payment_sequential"\
                                             ,"payment_type_id","payment_installments","payment_value", "order_status_id"\
                                             ,"product_product_id","seller_seller_id","customer_customer_id"\
                                             ,"shipping_limit_date_id","order_purchase_timestamp_id"\
                                             ,"order_approved_at_id","order_delivered_carrier_date_id"\
                                             , "order_delivered_customer_date_id","order_estimated_delivery_date_id")
        lisFactOrder = []

        for row in df_fact_order.collect():
            lisFactOrder.append({
                "order_sk":row.order_id,
                "order_item_sk":row.order_item_id,
                "price":row.price,
                "freight_value":row.freight_value,
                "payment_sequential":row.payment_sequential,
                "payment_type_id":row.payment_type_id,
                "payment_installments":row.payment_installments,
                "payment_value":row.payment_value,
                "order_status_id":row.order_status_id,
                "product_id":row.product_product_id,
                "seller_id":row.seller_seller_id,
                "customer_id":row.customer_customer_id,
                "shipping_limit_date_id":row.shipping_limit_date_id,
                "order_purchase_timestamp_id":row.order_purchase_timestamp_id,
                "order_approved_at_id":row.order_approved_at_id,
                "order_delivered_carrier_date_id":row.order_delivered_carrier_date_id,
                "order_delivered_customer_date_id":row.order_delivered_customer_date_id,
                "order_estimated_delivery_date_id":row.order_estimated_delivery_date_id
            })

            stmt = """
            Insert Into fact_order(order_sk, order_item_sk,price,freight_value,payment_sequential, 
                                        payment_type_id, payment_installments, payment_value, order_status_id,
                                        product_id,seller_id,customer_id,shipping_limit_date_id,
                                        order_purchase_timestamp_id,order_approved_at_id,order_delivered_carrier_date_id,
                                        order_delivered_customer_date_id, order_estimated_delivery_date_id)
                Values (%(order_sk)s,%(order_item_sk)s,%(price)s,%(freight_value)s,%(payment_sequential)s,%(payment_type_id)s,
                                        %(payment_installments)s,%(payment_value)s,%(order_status_id)s,%(product_id)s,
                                        %(seller_id)s,%(customer_id)s,%(shipping_limit_date_id)s,
                                        %(order_purchase_timestamp_id)s,%(order_approved_at_id)s,%(order_delivered_carrier_date_id)s,
                                        %(order_delivered_customer_date_id)s, %(order_estimated_delivery_date_id)s)
            """
        self.conn.cursor().executemany(stmt, lisFactOrder)
        self.conn.commit()

        print('Records successfully inserted')

    """
         O método lisAllDimGeolocation retorna todos os 
         registros da dimensões de geolocalizações.
    """
    def lisAllDimGeolocation(self):
        stmtDimGeo = """
                    select 
                        geolocation_id, 
                        geolocation_zip_code_prefix_sk  
                    from  dim_geolocation 
                        """
        dim_geolocation = self.conn.cursor()
        dim_geolocation.execute(stmtDimGeo)
        dim_geolocation = dim_geolocation.fetchall()

        return dim_geolocation

    """
        O método listAllDimLocalication retorna todos os 
        registros da dimensões de localização.
    """
    def listAllDimLocalication(self):
        stmtDimLocalication = """
                           select 
                                localization_id, 
                                localization_zip_code_prefix 
                            from  dim_localization;
                               """
        dim_localization = self.conn.cursor()
        dim_localization.execute(stmtDimLocalication)
        dim_localization = dim_localization.fetchall()

        return dim_localization

    """
        O método listAllDimCustomer retorna todos os 
        registros da dimensões de clientes.
    """
    def listAllDimCustomer(self):
        stmtDimCustomer = """
                           select 
                                customer_id, 
                                customer_sk 
                            from  dim_customer;
                               """
        dim_customer = self.conn.cursor()
        dim_customer.execute(stmtDimCustomer)
        dim_customer = dim_customer.fetchall()

        return dim_customer

    """
        O método listAllDimTime retorna todos os 
        registros da dimensão tempo.
    """
    def listAllDimTime(self):
        stmtDimTime = """
                           select 
                                time_id, 
                                time_date 
                            from  dim_time;
                               """
        dim_time = self.conn.cursor()
        dim_time.execute(stmtDimTime)
        dim_time = dim_time.fetchall()

        return dim_time

    """
        O método listAllDimProduct retorna todos os 
        registros da dimensão produto.
    """
    def listAllDimProduct(self):
        stmtDimProduct= """
                              select 
                                   product_id,
                                   product_sk
                               from  dim_product;
                                  """
        dim_product = self.conn.cursor()
        dim_product.execute(stmtDimProduct)
        dim_product = dim_product.fetchall()

        return dim_product

    """
        O método listAllDimOrderStatur retorna todos os 
        registros da dimensão estatus de order.
    """
    def listAllDimOrderStatur(self):
        stmtDimOrderStatus = """
                                 select 
                                      order_status_id,
                                      order_status_description
                                  from  dim_order_status;
                                     """
        dim_order_status = self.conn.cursor()
        dim_order_status.execute(stmtDimOrderStatus)
        dim_order_status = dim_order_status.fetchall()


        return dim_order_status

    """
        O método listAllDimPaymentType retorna todos os 
        registros da dimensão tipos de pagamentos.
    """
    def listAllDimPaymentType(self):
        stmtDimPaymentType = """
                                 select 
                                      payment_type_id,
                                      payment_type_description
                                  from  dim_payment_type;
                                     """
        dim_payment_type = self.conn.cursor()
        dim_payment_type.execute(stmtDimPaymentType)
        dim_payment_type = dim_payment_type.fetchall()

        return dim_payment_type

    """
        O método listAllDimSeller retorna todos os 
        registros da dimensão vendedor.
    """
    def listAllDimSeller(self):
        stmtDimSeller = """
                            select 
                                seller_id,
                                seller_sk
                            from  dim_seller;
                                        """
        dim_seller = self.conn.cursor()
        dim_seller.execute(stmtDimSeller)
        dim_seller = dim_seller.fetchall()

        return dim_seller

    """
        O método listAllDimCategoryProduct retorna todos os 
        registros da dimensão categoria de produtos.
    """
    def listAllDimCategoryProduct(self):
        stmtDimCatProd = """
                            select 
                                category_product_id,
                                category_product_name 
                            from dim_category_product;
                            """
        dim_catProd = self.conn.cursor()
        dim_catProd.execute(stmtDimCatProd)
        dim_catProd = dim_catProd.fetchall()

        return dim_catProd








