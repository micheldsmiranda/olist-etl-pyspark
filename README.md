# olist-etl-pyspark

Este repositório armazena o projeto de ETL desenvolvido através da ferramenta Apache Spark.
Para usar o projeto é preciso você utilizar um ambiente virtual. Para isto, execute os comandos como no exemplo abaixo:

`pip3 install virtualenv`

`virtualenv venv -p python3`

`source venv/bin/activate `

`git clone https://gitlab.com/micheldsmiranda/olist-etl-pyspark.git`

`cd olist-etl-pyspark`

`pip install -r requirements.txt `
 