import mysql.connector

"""
    A classe ConnectDbMySQL é responsável em realizar 
    a conexão com o banco de dados.
"""
class ConnectDbMySQL:
    def __init__(self, user, password, database, host, port):
        self.user = user
        self.password = password
        self.database = database
        self.host = host
        self.port = port

    def start(self):
        print('Starting connection Bd')
        db = mysql.connector.MySQLConnection(user=self.user,
                                             password=self.password,
                                             host=self.host,
                                             port=self.port,
                                             database=self.database)
        print('Successful connection Bd')
        return db
